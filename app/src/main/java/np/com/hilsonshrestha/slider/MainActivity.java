package np.com.hilsonshrestha.slider;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    SliderView slider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        slider = (SliderView) findViewById(R.id.rangeSlider);

        slider.setChangeListener(new SliderView.onChangeListener () {
            public void execute() {
                Log.e("TTT", slider.defaultMax + " " + slider.defaultMin);
            }
        });
    }
}
