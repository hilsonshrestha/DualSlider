package np.com.hilsonshrestha.slider;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Hilson on 10/10/2016.
 */
public class SliderView extends View {
    private int circleColor, circleColorSelected;
    float rangeMin, rangeMax, defaultMin, defaultMax;
    private String sliderLabel, sliderUnit;
    private Paint circlePaint, circlePaintSelected;

    float viewWidthHalf, viewHeightHalf, viewHeight, viewWidth;
    float sliderWidth;

    float circleRadius;
    float circleRadiusSelected;

    private static String TAG = "LOG";

    SliderCircle sliderCircle1, sliderCircle2;

    private float dp10;



    public interface onChangeListener {
        void execute();
    }
    private onChangeListener changeListener;

    public void setChangeListener(onChangeListener command) {
        changeListener = command;
    }


    class SliderCircle {
        public float value, min, max;
        public boolean selected = false;
        public float screenValue, screenMin, screenMax;

        public SliderCircle(float value, float min, float max) {
            this.value = value;
            this.min = min;
            this.max = max;
            this.screenValue = sliderWidth * (value - min) / (max - min);
            this.screenMin = dp10;
            this.screenMax = dp10 + sliderWidth * (max - min) / (max - min);
        }

        public void updateScreenValue(float screenValue) {
            if (screenValue < screenMin) {
                screenValue = screenMin;
            } else if (screenValue > screenMax) {
                screenValue = screenMax;
            }
            updateX(screenValue);
        }

        private float getX() {
            return screenValue;
        }

        private void updateX(float screenValue) {
            this.screenValue = screenValue;
            this.value = (screenValue - dp10) * (rangeMax - rangeMin) / sliderWidth + rangeMin;
        }

        public void sliderWidthUpdate() {
            screenValue = sliderWidth * (value - min) / (max - min);
            screenMax = dp10 + sliderWidth;// * (max - min) / (rangeMax - rangeMin);
        }

        public void draw(Canvas canvas) {
            circlePaint.setStyle(Paint.Style.FILL);
            circlePaint.setAntiAlias(true);

            circlePaint.setColor(circleColor);
            if (selected) {
                canvas.drawCircle(this.getX(), viewHeightHalf, circleRadiusSelected, circlePaint);
            } else {
                canvas.drawCircle(this.getX(), viewHeightHalf, circleRadius , circlePaint);
            }
        }

    }


    public SliderView(Context context, AttributeSet attrs) {
        super(context, attrs);

        circlePaint = new Paint();
        circlePaintSelected = new Paint();

        //get the attributes specified in attrs.xml using the name we included
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.Slider, 0, 0);


        try {
            //get the text and colors specified using the names in attrs.xml
            circleColor = a.getInteger(R.styleable.Slider_circleColor, 0);
            circleColorSelected = a.getInteger(R.styleable.Slider_circleColorSelected, 0);
            sliderLabel = a.getString(R.styleable.Slider_label);
            sliderUnit = a.getString(R.styleable.Slider_unit);
            rangeMax = a.getFloat(R.styleable.Slider_range_max, 0);
            rangeMin = a.getFloat(R.styleable.Slider_range_min, 100);
            defaultMin = a.getFloat(R.styleable.Slider_default_min, 0);
            defaultMax = a.getFloat(R.styleable.Slider_default_max, 100);
        } finally {
            a.recycle();
        }



        dp10 = convertDpToPixel(10, getContext());
        circleRadius = convertDpToPixel(5, getContext());
        circleRadiusSelected = convertDpToPixel(20, getContext());


        sliderCircle1 = new SliderCircle(defaultMin, rangeMin, rangeMax);
        sliderCircle2 = new SliderCircle(defaultMax, rangeMin, rangeMax);

    }

    /*
     * Update slider circle position when slider width is updated
     */
    private void updateSliderWidth() {
        sliderCircle1.sliderWidthUpdate();
        sliderCircle2.sliderWidthUpdate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        sliderCircle1.draw(canvas);
        sliderCircle2.draw(canvas);

        circlePaint.setTextSize(convertDpToPixel(10, getContext()));
        canvas.drawText(sliderLabel + " (" + sliderUnit + ") : " + String.format(java.util.Locale.US, "%.2f", defaultMin) + " - " + String.format(java.util.Locale.US, "%.2f", defaultMax), dp10, dp10 + dp10, circlePaint);
        canvas.drawLine(dp10, viewHeightHalf, sliderWidth + dp10, viewHeightHalf, circlePaint);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        viewWidth = MeasureSpec.getSize(widthMeasureSpec);
        viewHeight = MeasureSpec.getSize(heightMeasureSpec);
        sliderWidth = viewWidth - dp10 * 2;

        updateSliderWidth();

        viewWidthHalf = viewWidth / 2;
        viewHeightHalf = viewHeight / 2  + dp10;
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    SliderCircle selectedCircle;

    @Override
    public boolean onTouchEvent(MotionEvent event){


        // Check for best circle that resembles the touch.
        if (selectedCircle == null) {
            if (Math.abs(event.getX() - sliderCircle1.getX()) < dp10) {
                selectedCircle = sliderCircle1;
            } else if (Math.abs(event.getX() - sliderCircle2.getX()) < dp10) {
                selectedCircle = sliderCircle2;
            }
        }

        if (selectedCircle != null) {
            selectedCircle.updateScreenValue(event.getX());
            if (event.getAction() == 2) {
                selectedCircle.selected = true;
            } else {
                selectedCircle.selected = false;
                selectedCircle = null;
            }
            changeListener.execute();
        }

        if (sliderCircle1.value > sliderCircle2.value) {
            defaultMax = sliderCircle1.value;
            defaultMin = sliderCircle2.value;
        } else {
            defaultMax = sliderCircle2.value;
            defaultMin = sliderCircle1.value;
        }

        super.onTouchEvent(event);

        this.invalidate();

        return true;
    }






    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }



}
